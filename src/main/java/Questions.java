import java.util.ArrayList;
import java.util.List;

public class Questions {
    private String question;
    private String[] answersSet;
    private String answer;

    public Questions(String question, List<String> answerChoice, String answer) {
        this.question = question;
        this.answersSet = new String[3];
        this.answer = answer;
        }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getAnswersSet() {
        return answersSet;
    }

    public void setAnswersSet(String[] answersSet) {
        this.answersSet = answersSet;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

